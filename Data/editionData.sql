INSERT INTO Edition VALUES (1, 'Symposium de Pataphysique Appliquée', 'Paris', TO_DATE('11-04-2017', 'DD-MM-YYYY HH24:MI:SS'), TO_DATE('15-04-2017', 'DD-MM-YYYY HH24:MI:SS'))
/
INSERT INTO Edition VALUES (2,'Symposium de Pataphysique Appliquée', 'New York', TO_DATE('15-10-2017', 'DD-MM-YYYY HH24:MI:SS'), TO_DATE('19-10-2017', 'DD-MM-YYYY HH24:MI:SS'))
/
INSERT INTO Edition VALUES (3,'SIAM Conference on Data Mining', 'Houston', TO_DATE('27-04-2017', 'DD-MM-YYYY HH24:MI:SS'), TO_DATE('29-04-2017', 'DD-MM-YYYY HH24:MI:SS'))
/
INSERT INTO Edition VALUES (4,'SIAM Conference on Data Mining', 'San Diego', TO_DATE('03-05-2018', 'DD-MM-YYYY HH24:MI:SS'), TO_DATE('05-05-2018', 'DD-MM-YYYY HH24:MI:SS'))
/