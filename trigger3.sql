CREATE OR REPLACE TRIGGER copresident_auteur_soumission
BEFORE INSERT OR UPDATE ON AuteurASoumission
REFERENCING NEW AS ligneApres
FOR EACH ROW 
DECLARE nbrCopresidentsAuteur INTEGER;
BEGIN SELECT COUNT(*)
	INTO nbrCopresidentsAuteur
    FROM CoPresident cp, ComiteRelecture cr, Soumission s
    WHERE cp.idComiteRelecture = cr.idComiteRelecture 
	 AND  cr.idTrack = s.idTrack 
	 AND  cp.idChercheur = :ligneApres.idChercheur 
	 AND  s.noSoumission = :ligneApres.noSoumission;
	IF nbrCopresidentsAuteur > 0
	THEN
    raise_application_error(-20101,'Un copresident ne peut pas etre un auteur de soumission!');
	END IF;
END;
/
