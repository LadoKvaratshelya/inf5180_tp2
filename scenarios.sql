
-- A.Scenarios pour les tables
-- pour vérifier le bon fonctionnement des contraintes d’intégrité de type identité (clé primaire), référentiel (clé étrangère) et de valeur non nulle. pour chaque table.

--------------------
-->Table Conference
--------------------
--test OK clé primaire titre
INSERT INTO Conference(titre, acronyme,frequence) VALUES ('Symposium de Pataphysique Appliquee', 'SPA', 6);

--test NOK, Raison: même clé primaire titre
INSERT INTO Conference(titre, acronyme,frequence) VALUES ('Symposium de Pataphysique Appliquee', 'AAA', 4);

--test NOK, Raison: clé primaire(titre) est null
INSERT INTO Conference(acronyme,frequence) VALUES ( 'SPA', 6);

--test NOK clé acronyme non unique
INSERT INTO Conference(titre, acronyme,frequence) VALUES ('SIAM Conference on Data Mining', 'SPA', 12);

--test NOK clé acronyme null
INSERT INTO Conference(titre,frequence) VALUES ('SIAM Conference on Data Mining', 12);

--test OK acronyme unique
INSERT INTO Conference(titre, acronyme,frequence) VALUES ('SIAM Conference on Data Mining', 'SDM', 12);

-----------------
-->Table Edition
-----------------
--test OK clé primaire non null et unique
INSERT INTO Edition(idEdition,titre,venue,dateDebut,dateFin) VALUES (1, 'Symposium de Pataphysique Appliquee', 'Paris', TO_DATE('11-04-2017', 'DD-MM-YYYY HH24:MI:SS'), TO_DATE('15-04-2017', 'DD-MM-YYYY HH24:MI:SS'));

--test NOK, Raison: clé primaire existe deja
INSERT INTO Edition(idEdition,titre,venue,dateDebut,dateFin) VALUES (1,'Symposium de Pataphysique Appliquee', 'New York', TO_DATE('15-10-2017', 'DD-MM-YYYY HH24:MI:SS'), TO_DATE('19-10-2017', 'DD-MM-YYYY HH24:MI:SS'));

--test NOK, Raison: clé primaire est null
INSERT INTO Edition(titre,venue,dateDebut,dateFin) VALUES ('SIAM Conference on Data Mining', 'Houston', TO_DATE('27-04-2017', 'DD-MM-YYYY HH24:MI:SS'), TO_DATE('29-04-2017', 'DD-MM-YYYY HH24:MI:SS'));

--test OK clé étrangère existe sur la table référencé
INSERT INTO Edition(idEdition,titre,venue,dateDebut,dateFin) VALUES (2,'Symposium de Pataphysique Appliquee', 'New York', TO_DATE('15-10-2017', 'DD-MM-YYYY HH24:MI:SS'), TO_DATE('19-10-2017', 'DD-MM-YYYY HH24:MI:SS'));

--test NOK, Raison: clé étrangère n'a pas de parent dnas la table référencé
INSERT INTO Edition(idEdition,titre,venue,dateDebut,dateFin) VALUES (3,'titre n esxite pas', 'New York', TO_DATE('15-10-2017', 'DD-MM-YYYY HH24:MI:SS'), TO_DATE('19-10-2017', 'DD-MM-YYYY HH24:MI:SS'));

--test NOK, Raison: clé étrangère est null
INSERT INTO Edition(idEdition,venue,dateDebut,dateFin) VALUES (4,'New York', TO_DATE('15-10-2017', 'DD-MM-YYYY HH24:MI:SS'), TO_DATE('19-10-2017', 'DD-MM-YYYY HH24:MI:SS'));
--autres tests OK clé primaire + clé étrangère
INSERT INTO Edition(idEdition,titre,venue,dateDebut,dateFin) VALUES (3,'SIAM Conference on Data Mining', 'Houston', TO_DATE('27-04-2017', 'DD-MM-YYYY HH24:MI:SS'), TO_DATE('29-04-2017', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO Edition(idEdition,titre,venue,dateDebut,dateFin) VALUES (4,'SIAM Conference on Data Mining', 'San Diego', TO_DATE('03-05-2018', 'DD-MM-YYYY HH24:MI:SS'), TO_DATE('05-05-2018', 'DD-MM-YYYY HH24:MI:SS'));

---------------
-->table Track
---------------
--test OK clé primaire idTrack non null et unique
INSERT INTO Track(idTrack,idEdition,titre,description,nbPapiersMin,nbPapiersMax) VALUES (1,1,'Biomedical','Applications biomedicales de la pataphysique',2,6);

--test NOK, Raison: clé primaire existe deja
INSERT INTO Track(idTrack,idEdition,titre,description,nbPapiersMin,nbPapiersMax) VALUES (1,1,'Biomedical','Applications biomedicales de la pataphysique',2,6);

--test NOK, Raison: clé primaire est null
INSERT INTO Track(idEdition,titre,description,nbPapiersMin,nbPapiersMax) VALUES (1,'Biomedical','Applications biomedicales de la pataphysique',2,6);

--test OK clé étrangère existe sur la table référencé
INSERT INTO Track(idTrack,idEdition,titre,description,nbPapiersMin,nbPapiersMax) VALUES (2,1,'Didactique','Didactique de la pataphysique',2,6);

--test NOK, Raison: clé étrangère n'a pas de parent dnas la table référencé
INSERT INTO Track(idTrack,idEdition,titre,description,nbPapiersMin,nbPapiersMax) VALUES (3,20,'edition inconnue','Didactique de la pataphysique',2,6);

--test NOK, Raison: description nulle 
INSERT INTO Track(idTrack,idEdition,titre,nbPapiersMin,nbPapiersMax) VALUES (3,1,'edition inconnue',2,6);

-- autres tests OK
INSERT INTO Track(idTrack,idEdition,titre,description,nbPapiersMin,nbPapiersMax) VALUES(3,2,'Didactique','Didactique avancee de la pataphysique',2,6);
INSERT INTO Track(idTrack,idEdition,titre,description,nbPapiersMin,nbPapiersMax) VALUES (4,2,'Informatique','Informatisation des outils pataphysiques',2,6);
INSERT INTO Track(idTrack,idEdition,titre,description,nbPapiersMin,nbPapiersMax) VALUES (5,3,'Graph Mining','Mining complex graphs',2,4);
INSERT INTO Track(idTrack,idEdition,titre,description,nbPapiersMin,nbPapiersMax) VALUES (6,3,'Deep Learning','Classification in the deep',2,4);
INSERT INTO Track(idTrack,idEdition,titre,description,nbPapiersMin,nbPapiersMax) VALUES (7,3,'Graph Mining','Mining heterogeneous networks',2,4);
INSERT INTO Track(idTrack,idEdition,titre,description,nbPapiersMin,nbPapiersMax) VALUES (8,3,'Deep Learning','Classification in the very deep',2,4);

---------------
-->table Sujet
---------------
--test OK clé primaire non null et unique
INSERT INTO Sujet(idSujet,motCle) VALUES (21,'homeopathie');

--test NOK, Raison: clé primaire existe deja
INSERT INTO Sujet(idSujet,motCle) VALUES (21,'autre');

--test NOK, Raison: clé primaire est null
INSERT INTO Sujet(idSujet,motCle) VALUES (null,'autre');

--test NOK, Raison: champ motCle null
INSERT INTO Sujet(idSujet,motCle) VALUES (22,null);

-- autres tests OK
INSERT INTO Sujet(idSujet,motCle) VALUES (22,'systemique');
INSERT INTO Sujet(idSujet,motCle) VALUES (23,'epidemiologie');
INSERT INTO Sujet(idSujet,motCle) VALUES (24,'primaire');
INSERT INTO Sujet(idSujet,motCle) VALUES (25,'didactique');
INSERT INTO Sujet(idSujet,motCle) VALUES (26,'psychanalyse');
INSERT INTO Sujet(idSujet,motCle) VALUES (27,'quantique');
INSERT INTO Sujet(idSujet,motCle) VALUES (28,'parallele');
INSERT INTO Sujet(idSujet,motCle) VALUES (29,'efficacite');
INSERT INTO Sujet(idSujet,motCle) VALUES (30,'hypergraphs');
INSERT INTO Sujet(idSujet,motCle) VALUES (31,'Big Data');
INSERT INTO Sujet(idSujet,motCle) VALUES (32,'heterogeneous networks');
INSERT INTO Sujet(idSujet,motCle) VALUES (33,'neuro-decision trees');
INSERT INTO Sujet(idSujet,motCle) VALUES (34,'neural nets');
INSERT INTO Sujet(idSujet,motCle) VALUES (35,'recurrent architectures');
INSERT INTO Sujet(idSujet,motCle) VALUES (36,'convolution');

--------------------
-->Table TrackASujet
--------------------
--test OK clé primaire(idTrack, idSujet) non null et unique
INSERT INTO TrackASujet(idTrack,idSujet) VALUES (1,21);

--test NOK, Raison: clé primaire(idTrack, idSujet) existe deja
INSERT INTO TrackASujet(idTrack,idSujet) VALUES (1,21);

--test NOK, Raison: un champ de la clé primaire est null
INSERT INTO TrackASujet(idTrack,idSujet) VALUES (null,21);

--test OK clé étrangère existe sur la table référencé
INSERT INTO TrackASujet(idTrack,idSujet) VALUES (1,22);

--test NOK, Raison: clé étrangère n'a pas de parent dnas la table référencé
INSERT INTO TrackASujet(idTrack,idSujet) VALUES (1,40);

-- autres tests OK
INSERT INTO TrackASujet(idTrack,idSujet) VALUES (1,23);
INSERT INTO TrackASujet(idTrack,idSujet) VALUES (2,24);
INSERT INTO TrackASujet(idTrack,idSujet) VALUES (2,25);
INSERT INTO TrackASujet(idTrack,idSujet) VALUES (2,26);
INSERT INTO TrackASujet(idTrack,idSujet) VALUES (3,24);
INSERT INTO TrackASujet(idTrack,idSujet) VALUES (3,25);
INSERT INTO TrackASujet(idTrack,idSujet) VALUES (3,26);
INSERT INTO TrackASujet(idTrack,idSujet) VALUES (4,27);
INSERT INTO TrackASujet(idTrack,idSujet) VALUES (4,28);
INSERT INTO TrackASujet(idTrack,idSujet) VALUES (4,29);
INSERT INTO TrackASujet(idTrack,idSujet) VALUES (5,30);
INSERT INTO TrackASujet(idTrack,idSujet) VALUES (5,31);
INSERT INTO TrackASujet(idTrack,idSujet) VALUES (5,32);
INSERT INTO TrackASujet(idTrack,idSujet) VALUES (6,34);
INSERT INTO TrackASujet(idTrack,idSujet) VALUES (6,35);
INSERT INTO TrackASujet(idTrack,idSujet) VALUES (6,29);
INSERT INTO TrackASujet(idTrack,idSujet) VALUES (6,36);
INSERT INTO TrackASujet(idTrack,idSujet) VALUES (7,30);
INSERT INTO TrackASujet(idTrack,idSujet) VALUES (7,31);
INSERT INTO TrackASujet(idTrack,idSujet) VALUES (7,33);
INSERT INTO TrackASujet(idTrack,idSujet) VALUES (8,34);
INSERT INTO TrackASujet(idTrack,idSujet) VALUES (8,35);
INSERT INTO TrackASujet(idTrack,idSujet) VALUES (8,36);
INSERT INTO TrackASujet(idTrack,idSujet) VALUES (8,31);

-------------------
-->Table Soumission
-------------------
--test OK clé primaire non null et unique
INSERT INTO Soumission (noSoumission,titre,resume,corps,idTrack) 
VALUES (1, 'La pataphysique homeopathique et ses bienfaits connus','bla-bla','tiny.url',1);

--test NOK, Raison: clé primaire existe deja
INSERT INTO Soumission (noSoumission,titre,resume,corps,idTrack) VALUES (1, 'La pataphysique homeopathique et ses bienfaits connus','bla-bla','tiny.url',1);

--test NOK, Raison: clé primaire est null
INSERT INTO Soumission (titre,resume,corps,idTrack) VALUES ('La pataphysique homeopathique et ses bienfaits connus','bla-bla','tiny.url',1);

--test OK clé étrangère existe sur la table référencé
INSERT INTO Soumission (noSoumission,titre,resume,corps,idTrack) VALUES (2, 'L’approche systemique en pataphysiologie','bla-bla','tiny.url',2);

--test NOK, Raison: clé étrangère idTrack=180 n'a pas de parent dnas la table référencé
INSERT INTO Soumission (noSoumission,titre,resume,corps,idTrack) VALUES (3, 'Revue critique des theories pataphydiques en epidemiologie','bla-bla','tiny.url',180);

--test NOK, Raison: champ titre null
INSERT INTO Soumission (noSoumission,titre,resume,corps,idTrack) VALUES (4, null,'bla-bla','tiny.url',2);

-- autres tests OK
INSERT INTO Soumission (noSoumission,titre,resume,corps,idTrack) VALUES (3, 'Revue critique des theories pataphydiques en epidemiologie','bla-bla','tiny.url',1);
INSERT INTO Soumission (noSoumission,titre,resume,corps,idTrack) VALUES (4, 'Enseignement de la pataphysique au primaire','bla-bla','tiny.url',2);
INSERT INTO Soumission (noSoumission,titre,resume,corps,idTrack) VALUES (5, 'Vers une nouvelle didactique de la pataphysique','bla-bla','tiny.url',1);
INSERT INTO Soumission (noSoumission,titre,resume,corps,idTrack) VALUES (6, 'Comparaison des cours de pataphysique et de psychanalyse','bla-bla','tiny.url',2);
INSERT INTO Soumission (noSoumission,titre,resume,corps,idTrack) VALUES (7, 'Education et pataphysique : un nouveau contrat social','bla-bla','tiny.url',3);
INSERT INTO Soumission (noSoumission,titre,resume,corps,idTrack) VALUES (8, 'Pataphysique : science, philosophie et escroquerie','bla-bla','tiny.url',3);
INSERT INTO Soumission (noSoumission,titre,resume,corps,idTrack) VALUES (9, 'Pate-a-sel et pataphysique : construire lenseignement du futur','bla-bla','tiny.url',3);
INSERT INTO Soumission (noSoumission,titre,resume,corps,idTrack) VALUES (10, 'Vers des outils pataphysiques adaptes : un etat de lart','bla-bla','tiny.url',4);
INSERT INTO Soumission (noSoumission,titre,resume,corps,idTrack) VALUES (11, 'Le calendrier pataphysique, une approche holistique' ,'bla-bla','tiny.url',4);
INSERT INTO Soumission (noSoumission,titre,resume,corps,idTrack) VALUES (12, 'Des outils imaginaires pour des solutions imaginaires','bla-bla','tiny.url',4);
INSERT INTO Soumission (noSoumission,titre,resume,corps,idTrack) VALUES (13, 'NewSQL : une approche relationelle solide pour une analyse de graphes robuste','bla-bla','tiny.url',5);
INSERT INTO Soumission (noSoumission,titre,resume,corps,idTrack) VALUES (14, 'Book embedding, reduction des espaces de recherche dans lanalyse de graphes complexes','bla-bla','tiny.url',5);
INSERT INTO Soumission (noSoumission,titre,resume,corps,idTrack) VALUES (15, 'Fonctions noyaux de graphes : extraction de caracteristiques dans des espaces infinis','bla-bla','tiny.url',5);
INSERT INTO Soumission (noSoumission,titre,resume,corps,idTrack) VALUES (16, 'Deep learning : une composition de problemes convexes et ses solutions alternatives','bla-bla','tiny.url',6);
INSERT INTO Soumission (noSoumission,titre,resume,corps,idTrack) VALUES (17, 'Linfluence des modeles distribues sur les langages de programmation','bla-bla','tiny.url',6);
INSERT INTO Soumission (noSoumission,titre,resume,corps,idTrack) VALUES (18, 'Cryptage a faible consommation denergie pour les table de partition dans les architectures distribuees','bla-bla','tiny.url',6);
INSERT INTO Soumission (noSoumission,titre,resume,corps,idTrack) VALUES (19, 'De lexploration des arbres suffixes','bla-bla','tiny.url',7);
INSERT INTO Soumission (noSoumission,titre,resume,corps,idTrack) VALUES (20, 'emulation de la tolerance aux pannes byzantines et des protocoles','bla-bla','tiny.url',7);
INSERT INTO Soumission (noSoumission,titre,resume,corps,idTrack) VALUES (21, 'Analyse de la verification du modele et de la redondance','bla-bla','tiny.url',7);
INSERT INTO Soumission (noSoumission,titre,resume,corps,idTrack) VALUES (22, 'Abyss : la singularite, une question de couches ?','bla-bla','tiny.url',8);
INSERT INTO Soumission (noSoumission,titre,resume,corps,idTrack) VALUES (23, 'Linfluence de la theorie aleatoire sur lingenierie du logiciel dautoapprentissage','bla-bla','tiny.url',8);
INSERT INTO Soumission (noSoumission,titre,resume,corps,idTrack) VALUES (24, 'Un cas pour la loi de Moore','bla-bla','tiny.url',8);
-------------------
-->Table Chercheur
-------------------
--test OK clé primaire non null et unique
INSERT INTO Chercheur(idChercheur,nom,prenom,adressse) VALUES (1, 'Deslisle', 'Xavier', 'UQAM');

--test NOK, Raison: clé primaire existe deja
INSERT INTO Chercheur(idChercheur,nom,prenom,adressse) VALUES (1, 'Laaroussi', 'Mohamed', 'UQAM');

--test NOK, Raison: champ adressse = null
INSERT INTO Chercheur(idChercheur,nom,prenom) VALUES (2, 'Laaroussi', 'Mohamed');

-- autres tests OK
INSERT INTO Chercheur(idChercheur,nom,prenom,adressse) VALUES (2, 'Capitaine', 'Milene', 'UQAM');
INSERT INTO Chercheur(idChercheur,nom,prenom,adressse) VALUES (3, 'Wouawad', 'Hajdi', 'U. de Provence');
INSERT INTO Chercheur(idChercheur,nom,prenom,adressse) VALUES (4, 'Dutoit', 'Renemut', 'Volkswagen');
INSERT INTO Chercheur(idChercheur,nom,prenom,adressse) VALUES (5, 'Strumpf', 'Helmut', 'Volkswagen');
INSERT INTO Chercheur(idChercheur,nom,prenom,adressse) VALUES (6, 'Gregoriano', 'Bruno', 'U. de Bologna');
INSERT INTO Chercheur(idChercheur,nom,prenom,adressse) VALUES (7, 'Missoukov', 'Vladimir', 'U. de Bologna');
INSERT INTO Chercheur(idChercheur,nom,prenom,adressse) VALUES (8, 'Nakamura', 'Tiropito', 'Sony');
INSERT INTO Chercheur(idChercheur,nom,prenom,adressse) VALUES (9, 'Blaassen', 'Wilem', 'Phillips');
INSERT INTO Chercheur(idChercheur,nom,prenom,adressse) VALUES (10, 'Fred', 'O’Henry', 'U. of Washington');
INSERT INTO Chercheur(idChercheur,nom,prenom,adressse) VALUES (11, 'Nora', 'Olssen', 'Ericsson');
INSERT INTO Chercheur(idChercheur,nom,prenom,adressse) VALUES (12, 'Jeyong', 'Park', 'Samsung');
INSERT INTO Chercheur(idChercheur,nom,prenom,adressse) VALUES (13, 'Rakesh', 'Gupta', 'SAP India');
INSERT INTO Chercheur(idChercheur,nom,prenom,adressse) VALUES (14, 'Ian', 'Holmes', 'Birkbeck College');
INSERT INTO Chercheur(idChercheur,nom,prenom,adressse) VALUES (15, 'Dolores', 'Cruz', 'U. de Madrid');
INSERT INTO Chercheur(idChercheur,nom,prenom,adressse) VALUES (16, 'Tadeusz', 'Hvostovski', 'U. of Krakov');
INSERT INTO Chercheur(idChercheur,nom,prenom,adressse) VALUES (17, 'Flanagan', 'David', 'U. of Krakov');
INSERT INTO Chercheur(idChercheur,nom,prenom,adressse) VALUES (18, 'Nassar', 'Nazih', 'U. of Krakov');
INSERT INTO Chercheur(idChercheur,nom,prenom,adressse) VALUES (19, 'Mario', 'Luigi', 'U. de Madrid');
INSERT INTO Chercheur(idChercheur,nom,prenom,adressse) VALUES (20, 'Trahan', 'Terry', 'U. of Washington');
INSERT INTO Chercheur(idChercheur,nom,prenom,adressse) VALUES (21, 'Auberjonois', 'Aubin', 'U. de Madrid');
INSERT INTO Chercheur(idChercheur,nom,prenom,adressse) VALUES (22, 'Jaeger', 'Patrick' , 'Sony');
INSERT INTO Chercheur(idChercheur,nom,prenom,adressse) VALUES (23, 'Wan', 'Chang', 'Sony');
INSERT INTO Chercheur(idChercheur,nom,prenom,adressse) VALUES (24, 'Pepin', 'Marsilius', 'U. of Washington');
INSERT INTO Chercheur(idChercheur,nom,prenom,adressse) VALUES (25, 'Ribeiro', 'Davi' , 'U. de Rio de Janeiro');
INSERT INTO Chercheur(idChercheur,nom,prenom,adressse) VALUES (26, 'Davison', 'Katherine', 'Birkbeck College');

--------------------------
-->Table AuteurASoumission
--------------------------
--test OK clé primaire non null et unique
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (1,1,1);

--test NOK, Raison: clé primaire(idChercheur, noSoumission) existe deja
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (1,1,2);

--test OK clé étrangère existe sur la  table référencé
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (3,1,2);

--test NOK, Raison: clé étrangère n'a pas de parent dnas la table référencé
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (1,130,2);

--test NOK, Raison: champ rangAuteur= null
INSERT INTO AuteurASoumission(idChercheur,noSoumission) VALUES (2,2);

-- autres tests OK
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (2,2,1);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (7,2,2);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (11,2,3);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (5,3,1);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (1,3,2);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (9,3,3);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (10,4,1);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (14,4,2);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (8,5,1);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (4,5,2);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (6,5,3);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (12,6,1);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (10,7,2);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (14,7,1);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (15,8,1);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (16,9,1);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (2,9,2);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (11,9,3);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (14,10,1);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (12,10,2);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (16,11,1);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (17,11,2);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (22,12,1);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (23,12,2);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (20,13,1);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (21,14,1);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (23,15,1);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (22,15,2);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (24,16,1);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (22,16,2);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (18,17,1);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (20,18,1);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (19,18,2);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (19,19,1);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (21,19,2);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (25,20,1);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (18,20,2);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (24,20,3);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (15,21,1);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (17,22,1);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (16,22,2);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (26,23,1);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (16,24,1);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (17,24,2);
INSERT INTO AuteurASoumission(idChercheur,noSoumission,rangAuteur) VALUES (18,24,3);

-------------------------
-->Table ComiteRelecture
-------------------------
--test OK clé primaire non null et unique
INSERT INTO ComiteRelecture(idComiteRelecture,idTrack) VALUES (11, 1);

--test NOK, Raison: clé primaire existe deja
INSERT INTO ComiteRelecture(idComiteRelecture,idTrack) VALUES (11, 2);

--test NOK, Raison: idTrack=1 existe deja (contrainte unique) 
INSERT INTO ComiteRelecture(idComiteRelecture,idTrack) VALUES (12, 1);

--test OK clé étrangère existe sur la  table référencé
INSERT INTO ComiteRelecture(idComiteRelecture,idTrack) VALUES (12, 2);

--test NOK, Raison: clé étrangère n'a pas de parent dnas la table référencé
INSERT INTO ComiteRelecture(idComiteRelecture,idTrack) VALUES (13, 200);

-- autres tests OK
INSERT INTO ComiteRelecture(idComiteRelecture,idTrack) VALUES (13, 3);
INSERT INTO ComiteRelecture(idComiteRelecture,idTrack) VALUES (14, 4);
INSERT INTO ComiteRelecture(idComiteRelecture,idTrack) VALUES (15, 5);
INSERT INTO ComiteRelecture(idComiteRelecture,idTrack) VALUES (16, 6);
INSERT INTO ComiteRelecture(idComiteRelecture,idTrack) VALUES (17, 7);
INSERT INTO ComiteRelecture(idComiteRelecture,idTrack) VALUES (18, 8);

--------------------
-->Table CoPresident
--------------------
--test OK clé primaire(idChercheur, idComiteRelecture) non null et unique
INSERT INTO CoPresident(idChercheur,idComiteRelecture) VALUES (1, 11);

--test NOK, Raison: clé primaire(idChercheur, idComiteRelecture) existe deja
INSERT INTO CoPresident(idChercheur,idComiteRelecture) VALUES (1, 11);

--test OK clé étrangères idChercheur et idComiteRelecture existent sur les  tables référencés
INSERT INTO CoPresident(idChercheur,idComiteRelecture) VALUES (4, 11);


--test NOK, Raison: clé étrangère idChercheur n'a pas de parent dnas la table référencé
INSERT INTO CoPresident(idChercheur,idComiteRelecture) VALUES (0, 17);

--test NOK, Raison: champ idChercheur null
INSERT INTO CoPresident(idChercheur,idComiteRelecture) VALUES (null, 11);
-- autres tests OK
INSERT INTO CoPresident(idChercheur,idComiteRelecture) VALUES (2, 12);
INSERT INTO CoPresident(idChercheur,idComiteRelecture) VALUES (3, 12);
INSERT INTO CoPresident(idChercheur,idComiteRelecture) VALUES (3, 13);
INSERT INTO CoPresident(idChercheur,idComiteRelecture) VALUES (2, 13);
INSERT INTO CoPresident(idChercheur,idComiteRelecture) VALUES (8, 14);
INSERT INTO CoPresident(idChercheur,idComiteRelecture) VALUES (4, 14);
INSERT INTO CoPresident(idChercheur,idComiteRelecture) VALUES (16, 15);
INSERT INTO CoPresident(idChercheur,idComiteRelecture) VALUES (10, 15);
INSERT INTO CoPresident(idChercheur,idComiteRelecture) VALUES (6, 16);
INSERT INTO CoPresident(idChercheur,idComiteRelecture) VALUES (8, 16);
INSERT INTO CoPresident(idChercheur,idComiteRelecture) VALUES (7, 17);
INSERT INTO CoPresident(idChercheur,idComiteRelecture) VALUES (8, 18);
INSERT INTO CoPresident(idChercheur,idComiteRelecture) VALUES (9, 18);


---------------
-->Table Membre
---------------
--test OK clé primaire (idChercheur, idComiteRelecture) non null et unique
INSERT INTO Membre(idComiteRelecture,idChercheur) VALUES (11, 3);

--test NOK, Raison: clé primaire(idChercheur, idComiteRelecture) existe deja
INSERT INTO Membre(idComiteRelecture,idChercheur) VALUES (11, 3);

--test OK clé étrangères idChercheur et idComiteRelecture existent sur les  tables référencés
INSERT INTO Membre(idComiteRelecture,idChercheur) VALUES (11,5);

--test NOK, Raison: clé étrangère idChercheur n'a pas de parent dnas la table référencé
INSERT INTO Membre(idComiteRelecture,idChercheur) VALUES (0,17);

--test NOK, Raison: champ idComiteRelecture null
INSERT INTO Membre(idComiteRelecture,idChercheur) VALUES (null,6);

-- autres tests OK
INSERT INTO Membre(idComiteRelecture,idChercheur) VALUES (11,6);
INSERT INTO Membre(idComiteRelecture,idChercheur) VALUES (12,1);
INSERT INTO Membre(idComiteRelecture,idChercheur) VALUES (12,4);
INSERT INTO Membre(idComiteRelecture,idChercheur) VALUES (12,7);
INSERT INTO Membre(idComiteRelecture,idChercheur) VALUES (13,4);
INSERT INTO Membre(idComiteRelecture,idChercheur) VALUES (13,1);
INSERT INTO Membre(idComiteRelecture,idChercheur) VALUES (13,7);
INSERT INTO Membre(idComiteRelecture,idChercheur) VALUES (14,3);
INSERT INTO Membre(idComiteRelecture,idChercheur) VALUES (14,9);
INSERT INTO Membre(idComiteRelecture,idChercheur) VALUES (14,11);
INSERT INTO Membre(idComiteRelecture,idChercheur) VALUES (15,12);
INSERT INTO Membre(idComiteRelecture,idChercheur) VALUES (15,8);
INSERT INTO Membre(idComiteRelecture,idChercheur) VALUES (15,14);
INSERT INTO Membre(idComiteRelecture,idChercheur) VALUES (16,13);
INSERT INTO Membre(idComiteRelecture,idChercheur) VALUES (16,11);
INSERT INTO Membre(idComiteRelecture,idChercheur) VALUES (16,9);
INSERT INTO Membre(idComiteRelecture,idChercheur) VALUES (17,8);
INSERT INTO Membre(idComiteRelecture,idChercheur) VALUES (17,14);
INSERT INTO Membre(idComiteRelecture,idChercheur) VALUES (17,15);
INSERT INTO Membre(idComiteRelecture,idChercheur) VALUES (18,13);
INSERT INTO Membre(idComiteRelecture,idChercheur) VALUES (18,9);
INSERT INTO Membre(idComiteRelecture,idChercheur) VALUES (18,2);

-------------------
-->Table Evaluation
-------------------
--test OK clé primaire(idChercheur, idComiteRelecture, noSoumission) non null et unique
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (11,5,1, 0);

--test NOK, Raison: clé primaire existe deja
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (11,5,1, 95);

--test OK clés étrangères (idChercheur, idComiteRelecture) et noSoumission existent sur les  tables référencés
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (11,3,2, 0);

--test NOK, Raison: clés étrangères (idChercheur, idComiteRelecture) et noSoumission  n'ont pas de parent dnas les tables référencés
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (101,0,222, 0);

--test NOK, Raison: champ noSoumission null
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (11,6,null,0);

-- autres tests OK
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (11,6,3, 0);
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (12,1,4, 0);
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (12,7,5, 0);
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (12,4,6, 0);
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (13,4,7, 0);
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (13,7,8, 0);
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (13,7,9, 0);
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (14,3,10, 0);
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (14,3,11, 0);
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (14,11,12, 0);
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (15,12,13, 0);
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (15,12,14, 0);
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (15,12,15, 0);
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (16,11,16, 0);
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (16,13,17, 0);
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (16,13,18, 0);
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (17,8,19, 0);
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (17,14,20, 0);
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (17,8,21, 0);
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (18,13,22, 0);
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (18,2,23, 0);
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (18,9,24, 0);
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (11,6,1, 0);
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (11,6,2, 0);
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (12,1,5, 0);
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (13,1,7, 0);
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (13,4,8, 0);
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (14,9,10, 0);
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (15,8,13, 0);
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (16,9,16, 0);
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (17,14,19, 0);
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (17,15,20, 0);
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (17,14,21, 0);
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (18,2,24, 0);
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (17,15,19, 0);


-- C.Scenarios pour les Checks
-- pour vérifier le bon fonctionnement des contraintes de type check
---------------------------
-->La contrainte checkDates
---------------------------
--test OK, cas dateDebut > dateFin on a deja inserer dans la table Edition avec succes
--test NOK, cas dateDebut > dateFin 
INSERT INTO Edition(idEdition,titre,venue,dateDebut,dateFin) VALUES (6, 'Symposium de Pataphysique Appliquee', 'Paris', TO_DATE('15-04-2017', 'DD-MM-YYYY'), TO_DATE('11-04-2017', 'DD-MM-YYYY'));

--test OK, cas dateDebut = dateFin
INSERT INTO Edition(idEdition,titre,venue,dateDebut,dateFin) VALUES (6, 'Symposium de Pataphysique Appliquee', 'Paris', TO_DATE('15-04-2017', 'DD-MM-YYYY'), TO_DATE('15-04-2017', 'DD-MM-YYYY'));

------------------------------
-->La contrainte checkNbPapier
------------------------------
--test OK, cas nbPapiersMax > nbPapiersMin, on a deja inserer dans la table Track avec succes

--test NOK, cas nbPapiersMin > nbPapiersMax 
INSERT INTO Track(idTrack,idEdition,titre,description,nbPapiersMin,nbPapiersMax) VALUES (9,2,'Biomedical','Applications biomedicales de la pataphysique',6,2);

--test NOK, cas nbPapiersMin = nbPapiersMax 
INSERT INTO Track(idTrack,idEdition,titre,description,nbPapiersMin,nbPapiersMax) VALUES (10,1,'Biomedical','Applications biomedicales de la pataphysique',2,2);

-- B.Scenarios pour triggers
-- pour vérifier le bon fonctionnement des triggers

-------------------------------
-->Trigger retrait_sans_souci
-------------------------------
--on affiche le nb de d'evaluations de la soumission #1 
select count(*) from Evaluation where noSoumission =1;
--on affiche le nb de d'AuteurASoumission de la soumission #1 
select count(*) from AuteurASoumission where noSoumission =1;

--On supprime la soumission#1
delete from soumission where noSoumission =1;

--on re-affiche le nb de d'evaluations de la soumission #1 doit etre =0
select count(*) from Evaluation where noSoumission =1;
--on re-affiche le nb de d'AuteurASoumission de la soumission #1 doit etre =0
select count(*) from AuteurASoumission where noSoumission =1;

-------------------------------
-->Trigger pas_conflits_interet
-------------------------------
--ajout du chercheur #3 dans le comite #18
INSERT INTO Membre(idComiteRelecture,idChercheur) VALUES (18,3);
--le chercheur#3 est auteur (rang 2) de la soumission #1 car on deja fait INSERT INTO AuteurASoumission VALUES (3,1,2);
--Le trigger doit lever une exception car le chercheur#3 est auteur (rang 2) de la soumission #1
INSERT INTO Evaluation(idComiteRelecture,idChercheur,noSoumission,note) VALUES (18,3,1, 0);

