INSERT INTO Soumission VALUES (1, 'La pataphysique homéopathique et ses bienfaits connus','bla-bla','tiny.url',1)
/
INSERT INTO Soumission VALUES (2, 'L’approche systémique en pataphysiologie','bla-bla','tiny.url',2)
/
INSERT INTO Soumission VALUES (3, 'Revue critique des théories pataphydiques en épidémiologie','bla-bla','tiny.url',1)
/
INSERT INTO Soumission VALUES (4, 'Enseignement de la pataphysique au primaire','bla-bla','tiny.url',2)
/
INSERT INTO Soumission VALUES (5, 'Vers une nouvelle didactique de la pataphysique','bla-bla','tiny.url',1)
/
INSERT INTO Soumission VALUES (6, 'Comparaison des cours de pataphysique et de psychanalyse','bla-bla','tiny.url',2)
/
INSERT INTO Soumission VALUES (7, 'Education et pataphysique : un nouveau contrat social','bla-bla','tiny.url',3)
/
INSERT INTO Soumission VALUES (8, 'Pataphysique : science, philosophie et escroquerie','bla-bla','tiny.url',3)
/
INSERT INTO Soumission VALUES (9, 'Pate-à-sel et pataphysique : construire lenseignement du futur','bla-bla','tiny.url',3)
/
INSERT INTO Soumission VALUES (10, 'Vers des outils pataphysiques adaptés : un état de lart','bla-bla','tiny.url',4)
/
INSERT INTO Soumission VALUES (11, 'Le calendrier pataphysique, une approche holistique' ,'bla-bla','tiny.url',4)
/
INSERT INTO Soumission VALUES (12, 'Des outils imaginaires pour des solutions imaginaires','bla-bla','tiny.url',4)
/
INSERT INTO Soumission VALUES (13, 'NewSQL : une approche relationelle solide pour une analyse de graphes robuste','bla-bla','tiny.url',5)
/
INSERT INTO Soumission VALUES (14, 'Book embedding, réduction des espaces de recherche dans lanalyse de graphes complexes','bla-bla','tiny.url',5)
/
INSERT INTO Soumission VALUES (15, 'Fonctions noyaux de graphes : extraction de caracteristiques dans des espaces infinis','bla-bla','tiny.url',5)
/
INSERT INTO Soumission VALUES (16, 'Deep learning : une composition de problèmes convexes et ses solutions alternatives','bla-bla','tiny.url',6)
/
INSERT INTO Soumission VALUES (17, 'Linfluence des modèles distribués sur les langages de programmation','bla-bla','tiny.url',6)
/
INSERT INTO Soumission VALUES (18, 'Cryptage à faible consommation dénergie pour les table de partition dans les architectures distribueés','bla-bla','tiny.url',6)
/
INSERT INTO Soumission VALUES (19, 'De lexploration des arbres suffixes','bla-bla','tiny.url',7)
/
INSERT INTO Soumission VALUES (20, 'Émulation de la tolérance aux pannes byzantines et des protocoles','bla-bla','tiny.url',7)
/
INSERT INTO Soumission VALUES (21, 'Analyse de la vérification du modèle et de la redondance','bla-bla','tiny.url',7)
/
INSERT INTO Soumission VALUES (22, 'Abyss : la singularité, une question de couches ?','bla-bla','tiny.url',8)
/
INSERT INTO Soumission VALUES (23, 'Linfluence de la théorie aléatoire sur lingénierie du logiciel dautoapprentissage','bla-bla','tiny.url',8)
/
INSERT INTO Soumission VALUES (24, 'Un cas pour la loi de Moore','bla-bla','tiny.url',8)
/