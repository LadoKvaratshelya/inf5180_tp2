CREATE OR replace TRIGGER pas_conflits_interet 
  BEFORE INSERT OR UPDATE 
	ON Evaluation 
	FOR EACH ROW 
DECLARE 
    nb_membres_conflit INTEGER; 
BEGIN 
    SELECT Count(*) 
    INTO   nb_membres_conflit 
    FROM   auteurasoumission aas 
    WHERE  aas.idchercheur = :new.idchercheur 
           AND aas.nosoumission = :new.nosoumission; 

    IF nb_membres_conflit > 0 THEN 
      Raise_application_error(-20104, 'Conflits d’intérêt - Aucun membre de comité n’évalue l’article d’un co-auteur'); 
	END IF; 
END; 
/ 