-- Index secondaire pour l'affichage de tracks d'une conférence donnée

	CREATE INDEX  Conf_titre_idx ON Edition (titre);
	
	
-- Index secondaire pour l'affichage des noms des membres du comité de relecture pour un track donné

	CREATE INDEX  Membre_nom_idx ON Membre (nom, idChercheur);

-- Index secondaire pour l'affichage du nombre d'articles soumis par track pour une conferénce donnée.

	CREATE INDEX Edition_Track_idx ON Track (idEdition, idTrack);
	
	CREATE INDEX Soumission_Track_idx ON Soumission (idTrack);


-- Index secondaire pour l'affichage du nombre moyen d'articles affectés à un membre pour un comité donné

	-- pas d'index secondaire
	
-- Liste des index

	
	SELECT DIC.TABLE_NAME, DIC.INDEX_NAME, DIC.column_name
    FROM   user_indexes MON_DIC, user_ind_columns DIC
    WHERE  DIC.TABLE_NAME  = MON_DIC.TABLE_NAME
    AND    DIC.INDEX_NAME  = MON_DIC.INDEX_NAME
    ORDER BY DIC.TABLE_NAME, DIC.INDEX_NAME, DIC.COLUMN_POSITION;