-- Affichage de tracks d'une conférence donnée
	SELECT idTrack
	FROM Edition e, Track t
	WHERE e.idEdition = t.idEdition and	e.titre = unTitre
	
-- Affichage des noms des membres du comité de relecture pour un track donné
	SELECT c.nom
	FROM ComiteRelecture cr, Membre m, Chercheur c
	WHERE cr.idComiteRelecture = m.idComiteRelecture AND
		  m.idChercheur = c.idChercheur AND	
	      cr.idTrack = unIdTrack;

-- Affichage du nombre d'articles soumis par track pour une conferénce donnée.
	SELECT Count(noSoumission) as nbArticlesSoumis, t.idTrack
	FROM Edition e, Track t, Soumission s
	WHERE e.idEdition = t.idEdition AND
	      t.idTrack = s.idTrack AND	
		  e.titre = unTitreConference
	GROUP BY t.idTrack;
	
	
-- Affichage du nombre moyen d'articles affectés à un membre pour un comité donné
    SELECT (Count(noSoumission) / Count(m.idChercheur)) as moyen
	FROM ComiteRelecture cr, Track t, Soumission s, Membre m
	WHERE cr.idTrack = t.idTrack AND
		  t.idTrack = s.idTrack AND	
		  cr.idComiteRelecture = m.idComiteRelecture AND
		  cr.idComiteRelecture = unIdComiteRelecture
	
	
	
	
	
	
	
	
	