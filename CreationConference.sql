SET ECHO ON
-- Script Oracle SQL*plus de creation du schema TP2 
-- Version sans accents

-- Creation des tables
SET ECHO ON
CREATE TABLE Conference
(
titre		VARCHAR(50) 	NOT NULL,
acronyme	VARCHAR(25) 	NOT NULL,
frequence	INTEGER 		NOT NULL,
PRIMARY KEY 	(titre),
CONSTRAINT uAcronyme UNIQUE (acronyme)
)
/
CREATE TABLE Edition
(
idEdition	INTEGER 		NOT NULL,
titre		VARCHAR(50) 	NOT NULL,
venue		VARCHAR(50) 	NOT NULL,
dateDebut	DATE			NOT NULL,
dateFin		DATE			NOT NULL,
--	CHECK (dateDebut <= dateFin),
PRIMARY KEY (idEdition),
FOREIGN KEY 	(titre) REFERENCES Conference
)
/
CREATE TABLE Track
(
idTrack			INTEGER 		NOT NULL,
idEdition		INTEGER 		NOT NULL,
titre			VARCHAR(256) 	NOT NULL,
description		VARCHAR(256) 	NOT NULL,
nbPapiersMin 	INTEGER 		NOT NULL,
nbPapiersMax 	INTEGER 		NOT NULL,
--	CHECK (nbPapiersMin < nbPapiersMax),
PRIMARY KEY (idTrack),
FOREIGN KEY 	(idEdition) REFERENCES Edition
)
/
CREATE TABLE Sujet
(
idSujet			INTEGER 		NOT NULL,
motCle			VARCHAR(50) 	NOT NULL,
PRIMARY KEY 	(idSujet)
)
/
CREATE TABLE TrackASujet
(
idTrack			INTEGER 		NOT NULL,
idSujet			INTEGER 		NOT NULL,
PRIMARY KEY (idTrack, idSujet),
FOREIGN KEY 	(idTrack) REFERENCES Track,
FOREIGN KEY 	(idSujet) REFERENCES Sujet
)
/
CREATE TABLE Soumission
(
noSoumission 	INTEGER 		NOT NULL,
titre			VARCHAR(256) 	NOT NULL,
resume			VARCHAR(50) 	NOT NULL,
corps			VARCHAR(50) 	NOT NULL,
idTrack			INTEGER 		NOT NULL,
PRIMARY KEY 	(noSoumission),
FOREIGN KEY 	(idTrack) REFERENCES Track
)
/
CREATE TABLE Chercheur
(
idChercheur			INTEGER 		NOT NULL,
nom					VARCHAR(50) 	NOT NULL,
prenom				VARCHAR(50) 	NOT NULL,
adressse			VARCHAR(50) 	NOT NULL,
PRIMARY KEY 	(idChercheur)
)
/
CREATE TABLE AuteurASoumission
(
idChercheur			INTEGER 		NOT NULL,
noSoumission		INTEGER 		NOT NULL,
rangAuteur			INTEGER 		NOT NULL,
PRIMARY KEY 	(idChercheur, noSoumission),
FOREIGN KEY 	(idChercheur) REFERENCES Chercheur,
FOREIGN KEY 	(noSoumission) REFERENCES Soumission
)
/
CREATE TABLE ComiteRelecture
(
idComiteRelecture	INTEGER 		NOT NULL,
idTrack				INTEGER 		NOT NULL,
CONSTRAINT Track_Unique UNIQUE (idTrack),
PRIMARY KEY 	(idComiteRelecture),
FOREIGN KEY 	(idTrack) REFERENCES Track
)
/
CREATE TABLE CoPresident
(
idChercheur			INTEGER 		NOT NULL,
idComiteRelecture	INTEGER 		NOT NULL,
PRIMARY KEY 	(idChercheur, idComiteRelecture),
FOREIGN KEY 	(idChercheur) REFERENCES Chercheur,
FOREIGN KEY 	(idComiteRelecture) REFERENCES ComiteRelecture
)
/
CREATE TABLE Membre
(
idComiteRelecture	INTEGER 		NOT NULL,
idChercheur			INTEGER 		NOT NULL,
PRIMARY KEY 	(idChercheur, idComiteRelecture),
FOREIGN KEY 	(idChercheur) REFERENCES Chercheur,
FOREIGN KEY 	(idComiteRelecture) REFERENCES ComiteRelecture
)
/
CREATE TABLE Evaluation
(
idComiteRelecture	INTEGER 		NOT NULL,
idChercheur			INTEGER 		NOT NULL,
noSoumission		INTEGER 		NOT NULL,
note				INTEGER,
PRIMARY KEY 	(idChercheur, idComiteRelecture, noSoumission),
FOREIGN KEY (idChercheur, idComiteRelecture) REFERENCES Membre,
FOREIGN KEY 	(noSoumission) REFERENCES Soumission
)
/
COMMIT
/