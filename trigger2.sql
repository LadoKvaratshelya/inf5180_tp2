CREATE OR REPLACE TRIGGER max_evaluation_par_membre 
BEFORE INSERT OR UPDATE ON Evaluation
REFERENCING NEW AS ligneApres
FOR EACH ROW 
DECLARE nbrEvaluations INTEGER;
BEGIN SELECT COUNT(*)
	INTO nbrEvaluations
    FROM Evaluation e
    WHERE e.idChercheur = :ligneApres.idChercheur
	  AND e.idComiteRelecture = :ligneApres.idComiteRelecture
	GROUP BY idChercheur;
	IF nbrEvaluations > 3
	THEN
    raise_application_error(-20101,'Le nombre evaluations maximale par membre a ete atteint!');
	END IF;
END;
/