INSERT INTO Track VALUES (1,1,'Biomedical','Applications biomédicales de la pataphysique',2,6)
/
INSERT INTO Track VALUES (2,1,'Didactique','Didactique de la pataphysique',2,6)
/
INSERT INTO Track VALUES (3,2,'Didactique','Didactique avancée de la pataphysique',2,6)
/
INSERT INTO Track VALUES (4,2,'Informatique','Informatisation des outils pataphysiques',2,6)
/
INSERT INTO Track VALUES (5,3,'Graph Mining','Mining complex graphs',2,4)
/
INSERT INTO Track VALUES (6,3,'Deep Learning','Classification in the deep',2,4)
/
INSERT INTO Track VALUES (7,3,'Graph Mining','Mining heterogeneous networks',2,4)
/
INSERT INTO Track VALUES (8,3,'Deep Learning','Classification in the very deep',2,4)
/