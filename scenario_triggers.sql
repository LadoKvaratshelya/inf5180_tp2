-- B.Scenarios pour triggers
-- pour vérifier le bon fonctionnement des triggers

--TRIGGER max_copresident_in_comite 
--Vérifie qu'il y a au plus deux co-presidents dans la table CoPresident qui appartiennent
-- au meme comité de relecture
--test OK: on insère dans la table CoPresident trois chercheurs, deux chercheurs pour idComiteRelecture = 11
--et le troisième pour idComiteRelecture = 12
--INSERT INTO CoPresident VALUES (1, 11);
--INSERT INTO CoPresident VALUES (4, 11);
I--NSERT INTO CoPresident VALUES (2, 12);

--test NOK, Raison: on insère dans la table trois chercheurs qui font partie du meme comité, idComiteRelecture = 11
--INSERT INTO CoPresident VALUES (1, 11);
--INSERT INTO CoPresident VALUES (4, 11);
INSERT INTO CoPresident VALUES (26, 11);

--TRIGGER max_evaluation_par_membre
--Vérifie qu'il y a au plus trois évaluations par membre de comité
--test OK: on insère dans la table Evaluation trois évaluations pour un memebre qui appartient
-- au meme comité de relecture
--INSERT INTO Evaluation VALUES (13,4,20, 0);
--INSERT INTO Evaluation VALUES (13,4,21, 0);
--INSERT INTO Evaluation VALUES (13,4,23, 0);

--test NOK, Raison: on insère dans la table Evaluation quatre évaluations pour un memebre qui appartient
-- au meme comité de relecture
--INSERT INTO Evaluation VALUES (13,4,19, 0);
--INSERT INTO Evaluation VALUES (13,4,20, 0);
INSERT INTO Evaluation VALUES (13,4,21, 0);
INSERT INTO Evaluation VALUES (13,4,23, 0);

--TRIGGER copresident_auteur_soumission
--Vérifie que Co-President de comité et Auteur de soumission sont incompatibles avec 
-- le meme track
--test OK: idCopresident = 9 et idChercheur = 7 dans le meme idTrack = 8
--INSERT INTO CoPresident VALUES (9, 18);
--INSERT INTO Track VALUES (8,3,'Deep Learning','Classification in the very deep',2,4);
--INSERT INTO Soumission VALUES (24, 'Un cas pour la loi de Moore','bla-bla','tiny.url',8);
--INSERT INTO AuteurASoumission VALUES (7,24,1);

--test NOK, idCopresident = 8 et idChercheur = 8 dans le meme idTrack = 8
--INSERT INTO CoPresident VALUES (8, 18);
--INSERT INTO Track VALUES (8,3,'Deep Learning','Classification in the very deep',2,4);
--INSERT INTO Soumission VALUES (24, 'Un cas pour la loi de Moore','bla-bla','tiny.url',8);
INSERT INTO AuteurASoumission VALUES (8,24,1);