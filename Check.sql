ALTER TABLE Edition
ADD CONSTRAINT checkDates
CHECK ((dateDebut <= dateFin))
/
ALTER TABLE Track
ADD CONSTRAINT checkNbPapier
CHECK (nbPapiersMin < nbPapiersMax)
/