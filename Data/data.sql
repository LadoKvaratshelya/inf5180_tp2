--1. Sujet
INSERT INTO Sujet VALUES (21,'homeopathie');
INSERT INTO Sujet VALUES (22,'systemique');
INSERT INTO Sujet VALUES (23,'epidemiologie');
INSERT INTO Sujet VALUES (24,'primaire');
INSERT INTO Sujet VALUES (25,'didactique');
INSERT INTO Sujet VALUES (26,'psychanalyse');
INSERT INTO Sujet VALUES (27,'quantique');
INSERT INTO Sujet VALUES (28,'parallele');
INSERT INTO Sujet VALUES (29,'efficacite');
INSERT INTO Sujet VALUES (30,'hypergraphs');
INSERT INTO Sujet VALUES (31,'Big Data');
INSERT INTO Sujet VALUES (32,'heterogeneous networks');
INSERT INTO Sujet VALUES (33,'neuro-decision trees');
INSERT INTO Sujet VALUES (34,'neural nets');
INSERT INTO Sujet VALUES (35,'recurrent architectures');
INSERT INTO Sujet VALUES (36,'convolution');

--2. Conference
INSERT INTO Conference VALUES ('Symposium de Pataphysique Appliquee', 'SPA', 6);
INSERT INTO Conference VALUES ('SIAM Conference on Data Mining', 'SDM', 12);

--3. Edition
INSERT INTO Edition VALUES (1, 'Symposium de Pataphysique Appliquee', 'Paris', TO_DATE('11-04-2017', 'DD-MM-YYYY HH24:MI:SS'), TO_DATE('15-04-2017', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO Edition VALUES (2,'Symposium de Pataphysique Appliquee', 'New York', TO_DATE('15-10-2017', 'DD-MM-YYYY HH24:MI:SS'), TO_DATE('19-10-2017', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO Edition VALUES (3,'SIAM Conference on Data Mining', 'Houston', TO_DATE('27-04-2017', 'DD-MM-YYYY HH24:MI:SS'), TO_DATE('29-04-2017', 'DD-MM-YYYY HH24:MI:SS'));
INSERT INTO Edition VALUES (4,'SIAM Conference on Data Mining', 'San Diego', TO_DATE('03-05-2018', 'DD-MM-YYYY HH24:MI:SS'), TO_DATE('05-05-2018', 'DD-MM-YYYY HH24:MI:SS'));

--4. Track
INSERT INTO Track VALUES (1,1,'Biomedical','Applications biomedicales de la pataphysique',2,6);
INSERT INTO Track VALUES (2,1,'Didactique','Didactique de la pataphysique',2,6);
INSERT INTO Track VALUES (3,2,'Didactique','Didactique avancee de la pataphysique',2,6);
INSERT INTO Track VALUES (4,2,'Informatique','Informatisation des outils pataphysiques',2,6);
INSERT INTO Track VALUES (5,3,'Graph Mining','Mining complex graphs',2,4);
INSERT INTO Track VALUES (6,3,'Deep Learning','Classification in the deep',2,4);
INSERT INTO Track VALUES (7,3,'Graph Mining','Mining heterogeneous networks',2,4);
INSERT INTO Track VALUES (8,3,'Deep Learning','Classification in the very deep',2,4);

--5. Soumission
INSERT INTO Soumission VALUES (1, 'La pataphysique homeopathique et ses bienfaits connus','bla-bla','tiny.url',1);
INSERT INTO Soumission VALUES (2, 'L’approche systemique en pataphysiologie','bla-bla','tiny.url',2);
INSERT INTO Soumission VALUES (3, 'Revue critique des theories pataphydiques en epidemiologie','bla-bla','tiny.url',1);
INSERT INTO Soumission VALUES (4, 'Enseignement de la pataphysique au primaire','bla-bla','tiny.url',2);
INSERT INTO Soumission VALUES (5, 'Vers une nouvelle didactique de la pataphysique','bla-bla','tiny.url',1);
INSERT INTO Soumission VALUES (6, 'Comparaison des cours de pataphysique et de psychanalyse','bla-bla','tiny.url',2);
INSERT INTO Soumission VALUES (7, 'Education et pataphysique : un nouveau contrat social','bla-bla','tiny.url',3);
INSERT INTO Soumission VALUES (8, 'Pataphysique : science, philosophie et escroquerie','bla-bla','tiny.url',3);
INSERT INTO Soumission VALUES (9, 'Pate-a-sel et pataphysique : construire lenseignement du futur','bla-bla','tiny.url',3);
INSERT INTO Soumission VALUES (10, 'Vers des outils pataphysiques adaptes : un etat de lart','bla-bla','tiny.url',4);
INSERT INTO Soumission VALUES (11, 'Le calendrier pataphysique, une approche holistique' ,'bla-bla','tiny.url',4);
INSERT INTO Soumission VALUES (12, 'Des outils imaginaires pour des solutions imaginaires','bla-bla','tiny.url',4);
INSERT INTO Soumission VALUES (13, 'NewSQL : une approche relationelle solide pour une analyse de graphes robuste','bla-bla','tiny.url',5);
INSERT INTO Soumission VALUES (14, 'Book embedding, reduction des espaces de recherche dans lanalyse de graphes complexes','bla-bla','tiny.url',5);
INSERT INTO Soumission VALUES (15, 'Fonctions noyaux de graphes : extraction de caracteristiques dans des espaces infinis','bla-bla','tiny.url',5);
INSERT INTO Soumission VALUES (16, 'Deep learning : une composition de problemes convexes et ses solutions alternatives','bla-bla','tiny.url',6);
INSERT INTO Soumission VALUES (17, 'Linfluence des modeles distribues sur les langages de programmation','bla-bla','tiny.url',6);
INSERT INTO Soumission VALUES (18, 'Cryptage a faible consommation denergie pour les table de partition dans les architectures distribuees','bla-bla','tiny.url',6);
INSERT INTO Soumission VALUES (19, 'De lexploration des arbres suffixes','bla-bla','tiny.url',7);
INSERT INTO Soumission VALUES (20, 'emulation de la tolerance aux pannes byzantines et des protocoles','bla-bla','tiny.url',7);
INSERT INTO Soumission VALUES (21, 'Analyse de la verification du modele et de la redondance','bla-bla','tiny.url',7);
INSERT INTO Soumission VALUES (22, 'Abyss : la singularite, une question de couches ?','bla-bla','tiny.url',8);
INSERT INTO Soumission VALUES (23, 'Linfluence de la theorie aleatoire sur lingenierie du logiciel dautoapprentissage','bla-bla','tiny.url',8);
INSERT INTO Soumission VALUES (24, 'Un cas pour la loi de Moore','bla-bla','tiny.url',8);

--6. ComiteRelecture
INSERT INTO ComiteRelecture VALUES (11, 1)
 
INSERT INTO ComiteRelecture VALUES (12, 2);
INSERT INTO ComiteRelecture VALUES (13, 3);
INSERT INTO ComiteRelecture VALUES (14, 4);
INSERT INTO ComiteRelecture VALUES (15, 5);
INSERT INTO ComiteRelecture VALUES (16, 6);
INSERT INTO ComiteRelecture VALUES (17, 7);
INSERT INTO ComiteRelecture VALUES (18, 8);

--7. Chercheur
INSERT INTO Chercheur VALUES (1, 'Deslisle', 'Xavier', 'UQAM';
INSERT INTO Chercheur VALUES (2, 'Capitaine', 'Milene', 'UQAM');
INSERT INTO Chercheur VALUES (3, 'Wouawad', 'Hajdi', 'U. de Provence');
INSERT INTO Chercheur VALUES (4, 'Dutoit', 'Renemut', 'Volkswagen');
INSERT INTO Chercheur VALUES (5, 'Strumpf', 'Helmut', 'Volkswagen');
INSERT INTO Chercheur VALUES (6, 'Gregoriano', 'Bruno', 'U. de Bologna');
INSERT INTO Chercheur VALUES (7, 'Missoukov', 'Vladimir', 'U. de Bologna');
INSERT INTO Chercheur VALUES (8, 'Nakamura', 'Tiropito', 'Sony');
INSERT INTO Chercheur VALUES (9, 'Blaassen', 'Wilem', 'Phillips');
INSERT INTO Chercheur VALUES (10, 'Fred', 'O’Henry', 'U. of Washington');
INSERT INTO Chercheur VALUES (11, 'Nora', 'Olssen', 'Ericsson');
INSERT INTO Chercheur VALUES (12, 'Jeyong', 'Park', 'Samsung');
INSERT INTO Chercheur VALUES (13, 'Rakesh', 'Gupta', 'SAP India');
INSERT INTO Chercheur VALUES (14, 'Ian', 'Holmes', 'Birkbeck College');
INSERT INTO Chercheur VALUES (15, 'Dolores', 'Cruz', 'U. de Madrid');
INSERT INTO Chercheur VALUES (16, 'Tadeusz', 'Hvostovski', 'U. of Krakov');
INSERT INTO Chercheur VALUES (17, 'Flanagan', 'David', 'U. of Krakov');
INSERT INTO Chercheur VALUES (18, 'Nassar', 'Nazih', 'U. of Krakov');
INSERT INTO Chercheur VALUES (19, 'Mario', 'Luigi', 'U. de Madrid');
INSERT INTO Chercheur VALUES (20, 'Trahan', 'Terry', 'U. of Washington');
INSERT INTO Chercheur VALUES (21, 'Auberjonois', 'Aubin', 'U. de Madrid');
INSERT INTO Chercheur VALUES (22, 'Jaeger', 'Patrick' , 'Sony');
INSERT INTO Chercheur VALUES (23, 'Wan', 'Chang', 'Sony');
INSERT INTO Chercheur VALUES (24, 'Pepin', 'Marsilius', 'U. of Washington');
INSERT INTO Chercheur VALUES (25, 'Ribeiro', 'Davi' , 'U. de Rio de Janeiro');
INSERT INTO Chercheur VALUES (26, 'Davison', 'Katherine', 'Birkbeck College');

--8. CoPresident
INSERT INTO CoPresident VALUES (1, 11);
INSERT INTO CoPresident VALUES (4, 11);
INSERT INTO CoPresident VALUES (2, 12);
INSERT INTO CoPresident VALUES (3, 12);
INSERT INTO CoPresident VALUES (3, 13);
INSERT INTO CoPresident VALUES (2, 13);
INSERT INTO CoPresident VALUES (8, 14);
INSERT INTO CoPresident VALUES (4, 14);
INSERT INTO CoPresident VALUES (16, 15);
INSERT INTO CoPresident VALUES (10, 15);
INSERT INTO CoPresident VALUES (6, 16);
INSERT INTO CoPresident VALUES (8, 16);
INSERT INTO CoPresident VALUES (7, 17);
INSERT INTO CoPresident VALUES (0, 17);
INSERT INTO CoPresident VALUES (8, 18);
INSERT INTO CoPresident VALUES (9, 18);

--9. Membre
INSERT INTO Membre VALUES (11, 3);
INSERT INTO Membre VALUES (11,5);
INSERT INTO Membre VALUES (11,6);
INSERT INTO Membre VALUES (12,1);
INSERT INTO Membre VALUES (12,4);
INSERT INTO Membre VALUES (12,7);
INSERT INTO Membre VALUES (13,4);
INSERT INTO Membre VALUES (13,1);
INSERT INTO Membre VALUES (13,7);
INSERT INTO Membre VALUES (14,3);
INSERT INTO Membre VALUES (14,9);
INSERT INTO Membre VALUES (14,11);
INSERT INTO Membre VALUES (15,12);
INSERT INTO Membre VALUES (15,8);
INSERT INTO Membre VALUES (15,14);
INSERT INTO Membre VALUES (16,13);
INSERT INTO Membre VALUES (16,11);
INSERT INTO Membre VALUES (16,9);
INSERT INTO Membre VALUES (17,8);
INSERT INTO Membre VALUES (17,14);
INSERT INTO Membre VALUES (17,15);
INSERT INTO Membre VALUES (18,13);
INSERT INTO Membre VALUES (18,9);
INSERT INTO Membre VALUES (18,2);

--10. Evaluation
INSERT INTO Evaluation VALUES (11,5,1, 0);
INSERT INTO Evaluation VALUES (11,3,2, 0);
INSERT INTO Evaluation VALUES (11,6,3, 0);
INSERT INTO Evaluation VALUES (12,1,4, 0);
INSERT INTO Evaluation VALUES (12,7,5, 0);
INSERT INTO Evaluation VALUES (12,4,6, 0);
INSERT INTO Evaluation VALUES (13,4,7, 0);
INSERT INTO Evaluation VALUES (13,7,8, 0);
INSERT INTO Evaluation VALUES (13,7,9, 0);
INSERT INTO Evaluation VALUES (14,3,10, 0);
INSERT INTO Evaluation VALUES (14,3,11, 0);
INSERT INTO Evaluation VALUES (14,11,12, 0);
INSERT INTO Evaluation VALUES (15,12,13, 0);
INSERT INTO Evaluation VALUES (15,12,14, 0);
INSERT INTO Evaluation VALUES (15,12,15, 0);
INSERT INTO Evaluation VALUES (16,11,16, 0);
INSERT INTO Evaluation VALUES (16,13,17, 0);
INSERT INTO Evaluation VALUES (16,13,18, 0);
INSERT INTO Evaluation VALUES (17,8,19, 0);
INSERT INTO Evaluation VALUES (17,14,20, 0);
INSERT INTO Evaluation VALUES (17,8,21, 0);
INSERT INTO Evaluation VALUES (18,13,22, 0);
INSERT INTO Evaluation VALUES (18,2,23, 0);
INSERT INTO Evaluation VALUES (18,9,24, 0);
INSERT INTO Evaluation VALUES (11,6,1, 0);
INSERT INTO Evaluation VALUES (11,6,2, 0);
INSERT INTO Evaluation VALUES (12,1,5, 0);
INSERT INTO Evaluation VALUES (13,1,7, 0);
INSERT INTO Evaluation VALUES (13,4,8, 0);
INSERT INTO Evaluation VALUES (14,9,10, 0);
INSERT INTO Evaluation VALUES (15,8,13, 0);
INSERT INTO Evaluation VALUES (16,9,16, 0);
INSERT INTO Evaluation VALUES (17,14,19, 0);
INSERT INTO Evaluation VALUES (17,15,20, 0);
INSERT INTO Evaluation VALUES (17,14,21, 0);
INSERT INTO Evaluation VALUES (18,2,24, 0);
INSERT INTO Evaluation VALUES (17,15,19, 0);

--11. AuteurASoumission
INSERT INTO AuteurASoumission VALUES (1,1,1);
INSERT INTO AuteurASoumission VALUES (3,1,2);
INSERT INTO AuteurASoumission VALUES (2,2,1);
INSERT INTO AuteurASoumission VALUES (7,2,2);
INSERT INTO AuteurASoumission VALUES (11,2,3);
INSERT INTO AuteurASoumission VALUES (5,3,1);
INSERT INTO AuteurASoumission VALUES (1,3,2);
INSERT INTO AuteurASoumission VALUES (9,3,3);
INSERT INTO AuteurASoumission VALUES (10,4,1);
INSERT INTO AuteurASoumission VALUES (14,4,2);
INSERT INTO AuteurASoumission VALUES (8,5,1);
INSERT INTO AuteurASoumission VALUES (4,5,2);
INSERT INTO AuteurASoumission VALUES (6,5,3);
INSERT INTO AuteurASoumission VALUES (12,6,1);
INSERT INTO AuteurASoumission VALUES (10,7,2);
INSERT INTO AuteurASoumission VALUES (14,7,1);
INSERT INTO AuteurASoumission VALUES (15,8,1);
INSERT INTO AuteurASoumission VALUES (16,9,1);
INSERT INTO AuteurASoumission VALUES (2,9,2);
INSERT INTO AuteurASoumission VALUES (11,9,3);
INSERT INTO AuteurASoumission VALUES (14,10,1);
INSERT INTO AuteurASoumission VALUES (12,10,2);
INSERT INTO AuteurASoumission VALUES (16,11,1);
INSERT INTO AuteurASoumission VALUES (17,11,2);
INSERT INTO AuteurASoumission VALUES (22,12,1);
INSERT INTO AuteurASoumission VALUES (23,12,2);
INSERT INTO AuteurASoumission VALUES (20,13,1);
INSERT INTO AuteurASoumission VALUES (21,14,1);
INSERT INTO AuteurASoumission VALUES (23,15,1);
INSERT INTO AuteurASoumission VALUES (22,15,2);
INSERT INTO AuteurASoumission VALUES (24,16,1);
INSERT INTO AuteurASoumission VALUES (22,16,2);
INSERT INTO AuteurASoumission VALUES (18,17,1);
INSERT INTO AuteurASoumission VALUES (20,18,1);
INSERT INTO AuteurASoumission VALUES (19,18,2);
INSERT INTO AuteurASoumission VALUES (19,19,1);
INSERT INTO AuteurASoumission VALUES (21,19,2);
INSERT INTO AuteurASoumission VALUES (25,20,1);
INSERT INTO AuteurASoumission VALUES (18,20,2);
INSERT INTO AuteurASoumission VALUES (24,20,3);
INSERT INTO AuteurASoumission VALUES (15,21,1);
INSERT INTO AuteurASoumission VALUES (17,22,1);
INSERT INTO AuteurASoumission VALUES (16,22,2);
INSERT INTO AuteurASoumission VALUES (26,23,1);
INSERT INTO AuteurASoumission VALUES (16,24,1);
INSERT INTO AuteurASoumission VALUES (17,24,2);
INSERT INTO AuteurASoumission VALUES (18,24,3);


--12. TrackASujet
INSERT INTO TrackASujet VALUES (1,21);
INSERT INTO TrackASujet VALUES (1,22);
INSERT INTO TrackASujet VALUES (1,23);
INSERT INTO TrackASujet VALUES (2,24);
INSERT INTO TrackASujet VALUES (2,25);
INSERT INTO TrackASujet VALUES (2,26);
INSERT INTO TrackASujet VALUES (3,24);
INSERT INTO TrackASujet VALUES (3,25);
INSERT INTO TrackASujet VALUES (3,26);
INSERT INTO TrackASujet VALUES (4,27);
INSERT INTO TrackASujet VALUES (4,28);
INSERT INTO TrackASujet VALUES (4,29);
INSERT INTO TrackASujet VALUES (5,30);
INSERT INTO TrackASujet VALUES (5,31);
INSERT INTO TrackASujet VALUES (5,32);
INSERT INTO TrackASujet VALUES (6,34);
INSERT INTO TrackASujet VALUES (6,35);
INSERT INTO TrackASujet VALUES (6,29);
INSERT INTO TrackASujet VALUES (6,36);
INSERT INTO TrackASujet VALUES (7,30);
INSERT INTO TrackASujet VALUES (7,31);
INSERT INTO TrackASujet VALUES (7,33);
INSERT INTO TrackASujet VALUES (8,34);
INSERT INTO TrackASujet VALUES (8,35);
INSERT INTO TrackASujet VALUES (8,36);
INSERT INTO TrackASujet VALUES (8,31);