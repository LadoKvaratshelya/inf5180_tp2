CREATE OR REPLACE TRIGGER retrait_sans_souci
AFTER DELETE
	ON Soumission
	FOR EACH ROW

BEGIN
	-- Suppression des affectations (dans Evaluation_)
	DELETE FROM Evaluation
	WHERE noSoumission=:old.noSoumission;
	
	-- Suppression des rôles d’auteur (dans AuteurASoumission)
	DELETE FROM AuteurASoumission
	WHERE noSoumission=:old.noSoumission;
END;

/