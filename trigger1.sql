CREATE OR REPLACE TRIGGER max_copresident_in_comite 
BEFORE INSERT OR UPDATE ON CoPresident
REFERENCING NEW AS ligneApres
FOR EACH ROW 
DECLARE nbrCopresidents INTEGER;
BEGIN SELECT COUNT(*)
	INTO nbrCopresidents
    FROM CoPresident cp
    WHERE cp.idComiteRelecture = :ligneApres.idComiteRelecture;
	IF nbrCopresidents > 2
	THEN
    raise_application_error(-20101,'Trop de copresidents pour le comite de relecture!');
	END IF;
END;
/

   